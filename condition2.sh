#!/usr/bin/env bash	

set -x
###################
#created by: ofir shmuel
#purpose: learn conditions
#version: 0.0.1
###################

num1=5
num2=4

[ $num1 -gt $num2 ]; echo $? # integer grater then
[ $num1 -lt $num2 ]; echo $? # integer less then
[ $num1 -ge $num2 ]; echo $? # integer grater or equal then
[ $num1 -le $num2 ]; echo $? # integer less or equal
[ $num1 -ne $num2 ]; echo $? # integer not equal
[ $num1 -eq $num2 ]; echo $? # integer equal
