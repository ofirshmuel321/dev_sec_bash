#!/usr/bin/env bash	

#set -e
#set -u
#set -x
###################
#created by: ofir shmuel
#purpose: learn conditions
#version: 0.0.1
###################

user=$(cat /etc/passwd| grep ippsec|awk -F: '{print $3}')

[[ $user -eq 100 ]]; echo $?
