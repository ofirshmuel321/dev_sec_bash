

#question number 6
$ > tailing.txt
$ tail -f tailing.txt 
hello
world
#in the other shell:
$ echo hello >> tailing.txt 
$ echo world >> tailing.txt

#question number 7
$ cat > tennis.txt << ace
> Oranit Farhi
> Ofir Shmuel
> Nadav Smoler
> Kory Keren
> Asaf Tamir
> ace
$ cat tennis.txt 
 Oranit Farhi
 Ofir Shmuel
 Nadav Smoler
 Kory Keren
 Asaf Tamir


