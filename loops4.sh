#!/usr/bin/env bash

#########################
#created by: Ofir Shmuel
#Purpose: learning loops
#Version: 0.0.1
########################

index=0

while [[ $index -lt 100 ]]
do

	echo $index
	let index++
done
