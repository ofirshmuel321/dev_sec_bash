#!/usr/bin/env bash


function user_from_passwd(){
	name=$1
	grep $name /etc/passwd

}


while getopts ":a:f:c:h" opts;
	do
	case $opts in
		a) echo " -a was invoked and passed value of $opts"    ;;
		c) echo " -c was invoked and passed value of $opts"    ;;
		f)  user_from_passwd $opts    ;;
		h) echo " usage is -a value -f value -c value -h for help"    ;;
		*) echo " incorrect options provide"    ;;
	esac
	done
