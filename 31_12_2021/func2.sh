#!/usr/bin/env bash

#######################
#Created By : Ofir Shmuel
#Purpose: Learn Function Leason 2
#Version: 0.0.1
#######################

function check_root_home(){
	ls -ltR /root
}

# Function needs to do one job.

if [[ $EUID -eq 0 ]];then
	check_root_home
else

	echo "Please run script with root permission"

fi







