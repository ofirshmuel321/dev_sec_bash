#!/usr/bin/env bash

#######################
#Created By : Ofir Shmuel
#Purpose: Learn Function Leason 2
#Version: 0.0.1
#######################



function main(){
	deco "$@"
}


function hello_world(){
	line='################'
	printf "\n$line\n# %s\n$line\n" " Hello World"

}

function deco(){

	line='################'
	printf "\n$line\n# %s\n$line\n" " $@"

}


######################## DO NOT REMOVE ##########################
main "$@"



