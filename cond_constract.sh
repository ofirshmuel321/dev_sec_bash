#!/usr/bin/env bash
set -x
#####################################
#created by: Ofir Shmuel
#purpose: Learn condition structs
#version: 0.0.1
#date: 27.12.2021
####################################


var=$1

if [[ $var -ge 42 ]] && [[ $var -gt 45 ]] 
then
	echo "the meaning of lift is 42"
fi

if [[ $var -ge 42 ]] || [[ $var -gt 45 ]]
then
	echo "the meaning of life is 42"
fi






