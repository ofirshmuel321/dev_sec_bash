#!/usr/bin/env bash

#########################
#created by: Ofir Shmuel
#Purpose: learning loops
#Version: 0.0.1
########################

for index in $1 $2 $3 $4 $5
do
	sleep 1
	echo $index
done
