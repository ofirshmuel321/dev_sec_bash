#!/usr/bin/env/ bash	
###############################
#Create by:  Ofir Shmuel	
#Purpose: to work with arrays
#version: 0.0.1
################################

name=$1
lname=$2
age=$3

multi=($name $lname $age)

echo "Hello : My Name is ${multi[0]} ${multi[1]} and I am ${multi[2]}"
