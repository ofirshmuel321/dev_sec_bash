#!/usr/bin/env/ bash	
set -x
set -e
set -u
###############################
#Create by:  Ofir Shmuel	
#Purpose: to work with arrays
#version: 0.0.6
set -e
set -u
################################

name=$1
lname=$2
age=$3


multi=($name $lname $age)

echo "Hello : My Name is ${multi[0]} ${multi[1]} and I am ${multi[2]}"
