#!/usr/bin/env bash

#########################
#created by: Ofir Shmuel
#Purpose: learning loops
#Version: 0.0.1
########################

for index in 1 2 3 4 5 6 7
do
	echo $index
done
