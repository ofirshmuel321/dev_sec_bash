#!/usr/bin/env bash

#--------------------------
#created by: Ofir shmuel
#Purpose: Password Generator
#Version: 0.0.1
#--------------------------

echo "This is a simple passwrod generator"
echo "please enter the length of the password:"
read PASS_LENGTH

for p in $(seq 1 5);
do

	openssl rand -base64 48 | cut -c1-$PASS_LENGTH

done
