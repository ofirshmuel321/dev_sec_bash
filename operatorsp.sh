#!/usr/bin/env bash
set -x
###################
#created by: Ofir Shmuel
# examples of Operators in bash
#version: 0.0.1
############################

#logical Operators
#and &&
# or ||
# not ! 

echo 0 && echo 0 ; echo $?
echo 1 || eho 0 ; echo $?
! echo 0 && echo 0 ; echo $?

# Existance Operators
#exists -e
#exists and is regular file -f
#exsits and is directory -d
#exsits and is runnable -x
#exsits and is readable -r
#exsits and is writable -w
#does not has any value -z
#has some value -n
#exists and is symbolic link -L

echo '-e' && [[ -e /etc/passwd ]]; echo $?
echo '-f' && [[ -f /etc/passwd ]]; echo $?
echo '-r' && [[ -r /etc/passwd ]]; echo $?
echo '-w' && [[ -w /etc/passwd ]]; echo $?
echo '-d' && [[ -d /etc/passwd ]]; echo $?
echo '-x' && [[ -x /etc/passwd ]]; echo $?
echo '-L' && [[ -L /etc/passwd ]]; echo $?


echo '-z' && [[ -z $SHELL ]]; echo $?
echo '-n' && [[ -n $SHELL ]]; echo $?


