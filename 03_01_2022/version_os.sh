#!/usr/bin/env bash

######################
#Created By: Ofir Shmuel
#Purpose; output os version
#Version: 0.0.1
######################

set -o nounset		# Treat unset variables as an error

. /etc/os-release
. /etc/lsb-release

if [[ -z $VERSION_CODENAME ]];then
	echo "Variable is empty"
else
	echo $VERSION_CODENAME
fi	
