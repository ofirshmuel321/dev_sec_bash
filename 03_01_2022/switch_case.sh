#!/usr/bin/env bash

#########################
#Created By: Ofir Shmuel
#Purpose: case options
#Version: 0.0.1
#########################

set -o nounset

. /etc/*-release
. /etc/os-version

case  $VERSION_CODENAME in 
	buster) echo "you are working with debian 10" ;;
	bullseye) echo " you are working with debian 11" ;;
	stretch) echo "you are working with debian 8" ;;
	*) echo "this distro is not included" ;;
esac



