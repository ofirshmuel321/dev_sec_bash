#!/usr/bin/env bash

#############################
#Created By: Ofir Shmuel
#Purpose: Show The ip address 
#Version: 0.0.1
############################

# Shows ip address from internet:
#for iface in $(ifconfig)
#do
          #printf "$iface%s\n"
          #declare -a array_test=["$iface"]
#done
#for i in "${array_test[@]}"; do echo "$i"; done 

# ip -4 -br -addr show |awk '{print $1,$3}'

IP_INTERFACES=($(ip a|grep state|awk '{print $2}'|sed 's/://g'))
	function get_ip_by_interface(){
	interface=$1
	ip_address=$(ip address show $interface |grep -v inte6|grep inet|awk '{print $2}')

}

for inet in ${IP_INTERFACES[@]}
do
	get_ip_by_interface $inet
	printf "\n%s\t%s\n" $inet $ip_address
done












